// If Node, include 
var has_require = typeof require !== 'undefined'
var bf, rnd;

if( typeof bf === 'undefined' ) {
	if( has_require ) {
		bf = require('./buildFile');
	}
}

if( typeof rnd === 'undefined' ) {
	if( has_require ) {
		rnd = require('./rnd');
	}
}

var getRnd = rnd.noConflict()

// Rules for balgomet
// Measures, bars, beats

// The collection of notes
var coll = [
	[],
	[]
];



// Selection of key - not really bothered by being tethered to "guitar friendly" keys
// So each is equally likely
var keys = [
	"E",
	"F",
	"F#",
	"G",
	"G#",
	"A",
	"A#",
	"B",
	"C",
	"C#",
	"D",
	"D#",
];

// Parameter set
var s = {
	scale:[
		{p:1, s:[2, 2, 1, 2, 2, 2, 1]}, // trad western
	],
	mode:[
		{p:0.05, m:0}, // ionian
		{p:0.30, m:1}, // dorian
		{p:0.10, m:2}, // phyrigian
		{p:0.10, m:3}, // lydian
		{p:0.05, m:4}, // myxolydian
		{p:0.35, m:5}, // aeolian
		{p:0.05, m:6}, // locrian
	],
// relative sig chances
// revise this one!
	sig: [
		{p:0.5, g:[4, 4]},
		{p:0.20, g:[12, 2]}, // 2 means quaver (counting semiq's. This means 12/8
		{p:0.20, g:[6, 2]},
		{p:0.1, g:[3, 4]},
	],
// This is set around guitar's scope
// idx:0 = starting bottom D (assuming D standard)
// idx:1 = starting 2nd fret of D string
// idx:2 = starting  open top D
// idx:3 = starting 12th fret of top D
	oct: [
		0.25,
		0.25,
		0.25,
		0.25
	],
	// If there is an anacruusis, how many notes is it and what are their likelihoods?
	// Actually 0.75% of the time there is none
	pctAna: [
		0.750,
		0.075,
		0.075,
		0.050,
		0.050
	],
	// Pick a starting pitch: The rules apply to
	startPitch: [
		0.35,
		0.055,
		0.155,
		0.075,
		0.21,
		0.05,
		0.105		
	],
	startLen: [
		{p:0.01, l:48, i:0},  // dotted b
		{p:0.01, l:32, i:1}, // breve
		{p:0.01, l:24, i:2}, // dotted semi b
		{p:0.01, l:16, i:3}, // semibreve
		{p:0.01, l:12, i:4}, // dotted m
		{p:0.15, l:8, i:5}, // minim
		{p:0.10, l:6, i:6}, //dotted crot
		{p:0.25, l:4, i:7}, // crot
		{p:0.05, l:3, i:8}, // dotted q
		{p:0.22, l:2, i:9}, // quaver 
		{p:0.18, l:1, i:10} // semiq
	],
	nextMatrix: [
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]},  // dotted b
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]}, // breve
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]}, // dotted semi b
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]}, // semibreve
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]}, // dotted m
		{c:[0.20, 0.42, 0.38], l:[0.20, 0.60, 0.20], seqDirFollow:[0.5, 0.5]}, // minim
		{c:[0.20, 0.42, 0.38], l:[0.05, 0.75, 0.20], seqDirFollow:[0.5, 0.5]}, //dotted crot
		{c:[0.25, 0.20, 0.55], l:[0.25, 0.55, 0.20], seqDirFollow:[0.6, 0.4]}, // crot
		{c:[0.25, 0.30, 0.45], l:[0.05, 0.75, 0.20], seqDirFollow:[0.6, 0.4]}, // dotted q
		{c:[0.25, 0.25, 0.50], l:[0.55, 0.25, 0.20], seqDirFollow:[0.7, 0.3]}, // quaver 
		{c:[0.50, 0.01, 0.38], l:[0.60, 0.20, 0.20], seqDirFollow:[0.9, 0.2]} // semiq
	],
/*	Deprecated
	nextPitch:[
		0.05, // same 
		0.05, // random
		0.1 // sequential
	], */
	// 4 element Patterns and their probabilities
	pats: [
		{p:0.1, pat:[0,0,0,0], i:0},
		{p:0.2, pat:[1,0,0,0], i:1},
		{p:0.15, pat:[1,1,0,0], i:2},
		{p:0.1, pat:[1,1,1,0], i:3},
		{p:0.25, pat:[1,1,1,1], i:4},
		{p:0.05, pat:[0,1,0,0], i:5},
		{p:0.025, pat:[0,1,1,0], i:6},
		{p:0.05, pat:[0,1,1,1], i:7},
		{p:0.025, pat:[0,0,1,0], i:8},
		{p:0.025, pat:[0,0,1,1], i:9},
		{p:0.025, pat:[0,0,0,1], i:10}
	],
	// Start length of first note
	pctMelisma: [
		0.9,
		0.1
	],
	// Either a random note, or a direct match, or a note balanced to the current. 

	bStartPChoice: [
		{p:0.5, s:[0.6,0.01,0.15,0.02,0.2,0.01,0.01]},
		{p:0.5, s:[0.6,0.01,0.15,0.02,0.2,0.01,0.01]}
	]
};

// Build a first measure 

// var startMeasure = function (){
// 	for (var i=0; i<1000; i++){
// 		var stPitch = findItem (startPitch);
// 		$('#d').append("<p>" + stPitch + "</p>");

// 	}

// 	//var startPat = findItem(pats);
// };

var findItem = function (arr){
	var roll = getRnd();//Math.random();
	var tot = 0;
	var pick = -1;
	arr.forEach((item, idx)=>{
		var p = item.p?item.p:item;

		tot = tot + p;
		if(tot>roll && pick === -1){
			pick = idx;
		}
	});
	return pick;
};

// Find in an even distribution
var findRnd = function (arr){
	var roll = getRnd();
	var idx = Math.floor(roll * (arr.length - 1));
	return arr[idx];
};



// startMeasure();

var getStartPlace = function (){

	return {
		key:findRnd(keys),
		scale:s.scale[findItem(s.scale)],
		mode:s.mode[findItem(s.mode)],
		sig:s.sig[findItem(s.sig)],
		notes:[

		]
	}
};

var getFirst = function (){
	return {
			p: findItem(s.startPitch),
			l: s.startLen[findItem(s.startLen)],
			direction: 0
		};
};

var getNext = function (last){
	// options for pitch:
	// 0) same again
	// 1) Random
	// 2) Sequential
	// Fun really starts here
	// Look up what the choices are for following on the last
	var matr = s.nextMatrix[last.l.i];
	// clone previous
	var nt = JSON.parse(JSON.stringify(last));
	getNextP(last, nt, matr);

	getNextL(last, nt, matr)


	return nt;
	
};

var getNextP = function (last, nt, matr){
	var sel = findItem(matr.c);

	if(sel == 1){
		nt.p = findItem(s.startPitch);
	} else if(sel == 2){
		// need to work out direction of travel
		// TODO: this item should have elevated probability if the last transition was sequential
		// (store transition into last would be a way of tackling this)
		// then consider  direction. if last transition was sequential, 80% follow direction
		var follow = findItem(matr.seqDirFollow);
		if (follow){
			// TODO modulo to wrap. Octave consideration
			nt.p += last.direction;
		} else {
			// pick dir at rnd
			nt.p += findItem([0.5, 0.5])?1:-1;
		}
	}

	// set direction regardless. TODO: modulo's and octaves
	nt.direction = nt.p - last.p;

};

var getNextL = function (last, nt, matr){
	var sel = findItem(matr.l);
	if(sel == 1 || sel == 2){
		// what should sel == 2 mean?
		nt.l = s.startLen[findItem(s.startLen)];
	} 
}

var startMeasure = function (carrier, Measures, BarsPerMeasure){
	// Initialisation
	var start = getStartPlace();
	var diff = 0;

	for (var i=0; i<Measures; i++){	
		var m = {bars: []};

		for (var j=0; j<BarsPerMeasure; j++){
			var noteIdx = 0; // interbar influence?
			var b = {notes:[], tl: diff};
			var complete = false;

			var bLen = start.sig.g[0] * start.sig.g[1];
			while(!complete){
				
				var n = (noteIdx === 0)?getFirst():getNext(b.notes[noteIdx - 1]);
				b.tl += n.l.l;
				// Do we carry over length?
				if(b.tl > bLen){
					if(!findItem(s.pctMelisma)){
						// Most of the time we don't, replace n with a smaller
						while(b.tl > bLen){
							// Is it possible for this to go infinite?
							b.tl -= n.l.l;
							n.l = s.startLen[n.l.i + 1];
							b.tl += n.l.l;
						}
					} else {
						var x = 1;
					}
					diff = b.tl - bLen;
				}
				console.log(n)
				b.notes.push(n);

				if(b.tl >= bLen){

					complete= true;
				} 

				noteIdx++;
			}
			// Starting Octave
			m.oct = findItem(s.oct);
			console.log(b)
			//console.log(b)
			m.bars.push(b);
		}
		
		m.start = start;
	
		carrier.channels[0].measures.push(m);
	}


	getLowPart(carrier);

	if(typeof bf !== 'undefined'){
		bf.buildFile(carrier, keys);		
	}
};	

var getLowPart = function (carrier){

	// how shall we proceed?
	var start = findItem(s.bStartPChoice);
	var choice = s.bStartPChoice[start].s;
	if(start===0){
		// random
		var note = findItem(choice);
	} else {

		var note = findItem(choice);
	}

	carrier.channels[0].measures.forEach(function(m){
		m.bars.forEach(function (b){
			for (var i=0; i< b.notes; i++){
				// 
			}
		});
	});

}

var carrier = {channels:[{measures: []}],};

var params = {
	"Measures": 1,
	"BarsPerMeasure": 8

};

if(typeof angular !== 'undefined'){
	angular.module("cApp",[])
	.controller("cController",["$scope", function ($scope){

		$scope.params = params;

		 //console.log(Object.keys($scope.params));

		$scope.resultStr = "";
		$scope.startMeasure = startMeasure;
		$scope.carrier = carrier;
		$scope.startMeasure($scope.carrier, $scope.params.Measures, $scope.params.BarsPerMeasure);
		
	}]);
} else {
	startMeasure(carrier, params.Measures, params.BarsPerMeasure);
}







