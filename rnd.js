"use strict";

(function() {
	var root = this;
	var previous_rnd = root.rnd;
		var seed = 2;
	var rnd = function() {

	    var x = Math.sin(seed++) * 10000;
    	return x - Math.floor(x);
		//return Math.random();
	}

	rnd.noConflict = function() {
		root.rnd = previous_rnd;
		return rnd;
	}


	if( typeof exports !== 'undefined' ) {
		if( typeof module !== 'undefined' && module.exports ) {
		  exports = module.exports = rnd;
		}
		exports.rnd = rnd;
	} 
	else {
		root.rnd = rnd;
	}

  }).call(this);

