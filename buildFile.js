
var fs = require('fs');
var Midi = require('./node_modules/jsmidgen/lib/jsmidgen');

 const uuidv4 = require('uuid/v4');




// work out mode offset
// note offsets follow
var getKeyMod = function (m, keys){
	console.log(m.oct);
	
	// Start with octave offset
	var startNote = 12 * (m.oct + 2) + 16
	// work out key offset
	var idx = getKeyOffset(keys, m.start.key); 

	if(idx>=0){
		startNote += idx;

		startNote = getModeOffset(m.start.scale.s, m.start.mode.m, startNote);
		
		console.log("startNote: " + startNote +  "   key:" + m.start.key + "   mode: " + m.start.mode.m + "  scale: " + m.start.scale.s)
	}

	return startNote;
};

var sortMode = function (scale, mode){
	var i;
	var sortedMode = [];
	for (i=mode; i<scale.length; i++){
		sortedMode.push(scale[i]);
	}
	for (i=0; i<mode; i++){
		sortedMode.push(scale[i]);	
	}
	console.log("sortedMode")
	console.log(sortedMode)
	return sortedMode;
};

var getModeOffset = function (scale, mode, start){
	for (var i=0; i<mode; i++){
		console.log("getModeOffset   i:" + i + "  mode:" + mode + "  start: " + start + "  scale[i]:" + scale[i]); 
		start += scale[i];
	}
	return start;
};

var getKeyOffset = function (keys, key){
	var idx = -1
	keys.forEach(function(item, i){
		if( item === key){
			console.log("Item:   " +  item + "    key:" + key + " Found index: " + i)
			idx = i;
		}
	});
	return idx;
};

var getNoteOffset = function (mode, no){
	var offset = 0;
	console.log("getNoteOffset  mode:" + mode + "  no:" + no);
	for (var i=0; i<no; i++){
		// Wrap around
		var fr = mode[i % mode.length];
		console.log("getNoteOffset  i:" + i + "  mode[i]:" + fr);
		offset+=fr;
	}
	return offset;
};

var buildFile = function(carrier, keys){
	var file = new Midi.File();
	var track = new Midi.Track();
	file.addTrack(track);

	var start = getKeyMod(carrier.channels[0].measures[0], keys);
	var sortedMode = sortMode(carrier.channels[0].measures[0].start.scale.s, carrier.channels[0].measures[0].start.mode.m);
	// carrier 
	carrier.channels.forEach(function(channel){
		channel.measures.forEach(function(m){
			m.bars.forEach(function (b){
				b.notes.forEach(function(n){
					var note = getNoteOffset(sortedMode, n.p);
					console.log("start:" + start + "    note: " + note)
					track.addNote(0, start + note, 32*n.l.l);


				});
			});
		});
	});

	var uuid = uuidv4(); 

	uuid='temp';

	fs.writeFileSync(uuid + '.mid', file.toBytes(), 'binary');
	fs.writeFile(uuid + '.json', JSON.stringify(carrier), function(err) {
		if(err) {
			console.error("Could not write file: %s", err);
		}
	});


}


module.exports = {
	buildFile:buildFile
};

/*
	oct: [
		0.25,
		0.25,
		0.25,
		0.25
	],
	
0 = e2 = 12 * 2 + 16 = 40
1 = e3 = 12 * 3 + 16 = 52
2 = e4 = 12 * 4 + 16 = 64
3 = e5 = 12 * 5 + 16 = 76
*/ 

